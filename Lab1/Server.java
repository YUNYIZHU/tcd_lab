import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {
	
	
	public static void main(String[] args) throws IOException{
		ThreadPool Pool=ThreadPool.GetInstance();
		
		int PortNum=Integer.parseInt(args[0]);
		
		//int PortNum=6789;
		ServerSocket HomeSocket=new ServerSocket(PortNum);
		System.out.println("Server Starting...");
		Pool.Start();
		TerminationThread TT=new TerminationThread(Pool);
		TT.start();
		
		while(!Pool.isShutDown){
			//System.out.println(Pool.isShutDown);
			Socket socket=HomeSocket.accept();
			Pool.AddSocket(socket);	
		}
		
	}
	
	
	
}
