import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class Chatroom {
	private String Name;
	private int REFF;
	
	private List<UserInfo> ClientList=Collections.synchronizedList(new LinkedList<UserInfo>());
	
	public Chatroom(String name,int reff){
		this.Name=name;
		this.REFF=reff;
	}
	
	public synchronized void AddClient(UserInfo s){
		synchronized(this.ClientList){
			this.ClientList.add(s);
			this.ClientList.notify();
		}
	}
	public synchronized void DeleteClient(UserInfo s){
		synchronized(this.ClientList){
			this.ClientList.remove(s);
			this.ClientList.notify();
		}
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public int getREFF() {
		return REFF;
	}

	public void setREFF(int rEFF) {
		REFF = rEFF;
	}

	public List<UserInfo> getClientList() {
		return ClientList;
	}

	public void setClientList(List<UserInfo> clientList) {
		ClientList = clientList;
	}
	
}
