import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;


public class Client {
	private boolean isShutDown=false;
	private Socket socket;
	private BufferedReader InFromUser;
	private BufferedReader InFromServer;
	private DataOutputStream OutToServer;
	private ReaderThread readerThread;
	private WriterThread writerThread;
	public Client(Socket sk){
		this.readerThread=new ReaderThread();
		this.writerThread=new WriterThread();
		this.socket=sk;
		this.InFromUser=new BufferedReader(new InputStreamReader(System.in));
		try {
			this.OutToServer=new DataOutputStream(socket.getOutputStream());
			//this.InFromServer=new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			this.InFromServer=new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void Start(){
		this.readerThread.start();
		this.writerThread.start();
	}
	public void Destory() throws IOException{
		this.socket.close();
	}
	private class ReaderThread extends Thread{       //private Thread for reading from server

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			try {
				ReadFromServer();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				
			}
		}
		private void ReadFromServer() throws IOException{
			while(!isShutDown){
				String str;
				if((str=InFromServer.readLine())!=null)
					//if(str!=" ")
					System.out.println(str);
			}
		}
		
	}
	
	private class WriterThread extends Thread{     //private thread for writing from client

		@Override
		public void run() {
			// TODO Auto-generated method stub
			super.run();
			try {
				Write();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		private void Write() throws IOException{
			while(!isShutDown){
				//System.out.println("Writing something..");
				String context=InFromUser.readLine();
				OutToServer.writeBytes(context+'\n');
				String[] content=context.split("\\s+");
				if(content[0].equalsIgnoreCase("DISCONNECT:")){
					try {
						Thread.sleep(1000);   // in case buffer
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					isShutDown=true;
				}
			}
		}
	}
	
//	public static void main(String[] args) throws UnknownHostException, IOException{
//		String str=null;
//		Socket socket=new Socket("Localhost",6789);
//		BufferedReader inFromUser=new BufferedReader(new InputStreamReader(System.in));
//		DataOutputStream outToServer=new DataOutputStream(socket.getOutputStream());
//		BufferedReader inFromServer=new BufferedReader(new InputStreamReader(socket.getInputStream()));
//		while(true){
//			String context,modifiedcontext;
//			modifiedcontext=inFromServer.readLine();
//			//while((modifiedcontext=inFromServer.readLine())!=null)
//			System.out.println(modifiedcontext);
//			System.out.println("Writing something");
//			
//			context=inFromUser.readLine();
//			outToServer.writeBytes(context+'\n');
//			if(context.equalsIgnoreCase("KillService")){
//				System.out.println("helo");
//				socket.close();
//				break;
//			}
//		}
//		
//	}

}
