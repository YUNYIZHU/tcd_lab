import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class ClientLauncher {
	public static void main(String[] args) throws UnknownHostException, IOException{
		//int PortNum=Integer.parseInt(args[0]);
		int PortNum=6789;
		Socket socket=new Socket("localhost",PortNum);
		Client c=new Client(socket);
		c.Start();
		
	}

}
