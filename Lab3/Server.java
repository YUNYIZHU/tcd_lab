import java.io.IOException;
import java.net.ServerSocket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class Server implements Runnable{
	private int Port=6789;
	private ServerSocket HomeSocket;
	private ExecutorService Pool;
	public static boolean isShutdown=false;
	public static LinkedList<Chatroom> RoomList=new LinkedList<Chatroom>();
	public static int IDAccounter=1;
	
	public Server() throws IOException{
		Chatroom NDSRoom=new Chatroom("NDS",1);
		this.RoomList.add(NDSRoom);
		this.HomeSocket=new ServerSocket(Port);
		Pool=Executors.newCachedThreadPool();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(!isShutdown){
			try {
				this.Pool.execute(new Worker(this.HomeSocket.accept()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("BYE BYE!");
		shutdownAndAwaitTermination(Pool);
		
		
	}
	
	private void shutdownAndAwaitTermination(ExecutorService pool) {
		   pool.shutdown(); // Disable new tasks from being submitted
		   try {
		     // Wait a while for existing tasks to terminate
		     if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
		       pool.shutdownNow(); // Cancel currently executing tasks
		       // Wait a while for tasks to respond to being cancelled
		       if (!pool.awaitTermination(60, TimeUnit.SECONDS))
		           System.err.println("Pool did not terminate");
		     }
		   } catch (InterruptedException ie) {
		     // (Re-)Cancel if current thread also interrupted
		     pool.shutdownNow();
		     // Preserve interrupt status
		     Thread.currentThread().interrupt();
		   }
	}
	
	

}
