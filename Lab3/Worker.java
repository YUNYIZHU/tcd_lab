import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


public class Worker implements Runnable{
	private Socket socket;
	//private String NickName;
	private BufferedReader InFromUser;
	private DataOutputStream OutToUser;
	private PrintWriter PW;
	private boolean isShutdown;
	private UserInfo User;
	
	public Worker(Socket s){
		this.socket=s;
		this.User=new UserInfo(s);
		
		this.isShutdown=false;
		try {
			InFromUser=new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
			OutToUser=new DataOutputStream(this.socket.getOutputStream());
			PW=new PrintWriter(OutToUser,true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(!this.isShutdown){
			//String[] content=new String[10];
			try {
				String[] content=InFromUser.readLine().split("\\s+");
				switch(content[0]){
				case "HELO": HELO(content);
							break;
				case "DISCONNECT:": KILL_SERVICE();
									break;
				case "JOIN_CHATROOM:": JoinChatroom(content);
									break;
				case "LEAVE_CHATROOM:": LeaveChatroom(content);
									break;
				case "CHAT:": SendMsg(content);
								break;
				default: break;
				
				}
//				try{
//					if(content[0].equalsIgnoreCase("KILL_SERVICE")){
//						
//						this.PW.println("Server is closing...");
//						
//						Server.isShutdown=true;
//						this.isShutdown=true;
//						System.exit(0);
//					}
//				}finally{
//					try {
//						this.OutToUser.close();
//						this.InFromUser.close();
//						
//						this.PW.close();
//						this.socket.close();
//						//System.out.println("12345");
//						try {
//							Thread.sleep(1000);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//						
//					
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						//e.printStackTrace();
//					}
//				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		//System.out.println("123");
		
		
	}
	private void HELO(String[] content){
		int StudentNUM=14307009;
		try {
			String IP=InetAddress.getLocalHost().getHostAddress();
			int Port=this.socket.getLocalPort();
			PW.println("HELO "+content[1]+'\n'+"IP:"+IP+'\n'+"Port:"+Port+'\n'+"StudentID:"+StudentNUM);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void KILL_SERVICE(){
		Server.isShutdown=true;
		this.isShutdown=true;
		try {
			
			this.PW.println("Server is closing...");
			
		} finally{
			try {
				this.OutToUser.close();
				this.InFromUser.close();
				
				this.PW.close();
				this.socket.close();
				//System.out.println("12345");
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.exit(0);
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void JoinChatroom(String[] content) throws UnknownHostException{
		this.User.setNickName(content[7]);
		this.User.setID(Server.IDAccounter);
		Server.IDAccounter++;
		for(Chatroom chatroom: Server.RoomList){
			if(content[1].equalsIgnoreCase(chatroom.getName())){
				if(!chatroom.getClientList().contains(this.User)){
					chatroom.AddClient(this.User);
					this.PW.println("JOINED_CHATROOM: "+chatroom.getName());
					this.PW.println("SERVER_IP: "+InetAddress.getLocalHost().getHostAddress());
					this.PW.println("PORT: "+this.socket.getLocalPort());
					this.PW.println("ROOM_REF: "+chatroom.getREFF());
					this.PW.println("JOIN_ID: "+this.User.getID());
					return;
					
				}
				
				Error(3,"you have joined this chatroom");
				return;
			}
		}
		Error(1,"Chatroom is not existed!");
		
		
	}
	private void LeaveChatroom(String[] content){
		//System.out.println(2);
		for(Chatroom chatroom: Server.RoomList){
			if(content[1].equals(Integer.toString(chatroom.getREFF()))){
					if(chatroom.getClientList().contains(this.User)){
					chatroom.DeleteClient(this.User);
					this.PW.println("LEFT_CHATROOM: "+chatroom.getREFF());
					this.PW.println("JOIN_ID: "+this.User.getID());
					//System.out.println(chatroom.getClientList().size());
					return;
				}
				Error(4,"you are not in this chatroom");
				return;
			}
		}
		Error(2,"Leave Failure");
	}
	
	private void Error(int i, String s){
		this.PW.println("ERROR_CODE: "+i);
		this.PW.println("ERROR_DESCRIPTION: "+s);
	}
	
	private synchronized void SendMsg(String[] content) throws IOException{
		//System.out.println(2);
		for(Chatroom chatroom: Server.RoomList){
			if(content[1].equals(Integer.toString(chatroom.getREFF()))){
				for(UserInfo u: chatroom.getClientList()){
					if(!u.equals(this.User)){
						DataOutputStream DOS=new DataOutputStream(u.getSokcet().getOutputStream());
						PrintWriter pw=new PrintWriter(DOS,true);
						pw.println("CHAT: "+chatroom.getREFF());
						pw.println("CLIENT_NAME: "+this.User.getNickName());
						pw.println("MESSAGE: "+content[7]);
						pw.println();
						System.out.println(1);
					}
					
				}
			}
		}
	}

}
