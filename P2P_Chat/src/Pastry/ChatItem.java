package Pastry;

public class ChatItem {
	private String chatContent;
	private String Origin_tag;
	public String getOrigin_tag() {
		return Origin_tag;
	}
	public void setOrigin_tag(String origin_tag) {
		Origin_tag = origin_tag;
	}
	public String getChatContent() {
		return chatContent;
	}
	public void setChatContent(String chatContent) {
		this.chatContent = chatContent;
	}
	
	
}
