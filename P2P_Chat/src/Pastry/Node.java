package Pastry;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

public class Node implements PeerChat{
	public static final int ROUTING_TABLE_SIZE=4;
	public  Node_Info node_Info;
	private UDPServer server;
	private UDPClient client;
	public Node_Info[] Routing_Table;
	public List<Node_Info> allNodes;

	@Override
	public void init(int Home_Port, String tag) throws SocketException, UnknownHostException {
		this.server=new UDPServer(Home_Port);
		
		this.node_Info=new Node_Info();
		this.node_Info.setTag(tag);
		this.node_Info.setNodeID(hashCode(tag));
		this.node_Info.setPort(Home_Port);
		Routing_Table=new Node_Info[4];
		allNodes=new ArrayList<Node_Info>();
//		this.Routing_Table[1]=this.node_Info;
		this.allNodes.add(this.node_Info);
		
		new Thread(this.server).start();
		
	}

	@Override
	public void joinNetwork(int Gateway_Port,String Gateway_Tag) {
		int Gateway_NodeID=hashCode(Gateway_Tag);
		Node_Info Gateway_NodeInfo=new Node_Info();
		Gateway_NodeInfo.setNodeID(Gateway_NodeID);
		Gateway_NodeInfo.setPort(Gateway_Port);
		Gateway_NodeInfo.setTag(Gateway_Tag);
		
		
		if(Gateway_NodeID>node_Info.getNodeID()){
			this.Routing_Table[2]=Gateway_NodeInfo;
		}else{
			this.Routing_Table[1]=Gateway_NodeInfo;
		}
		
		
		
		PastryMessage message=new PastryMessage();
		message.setType("JOINING_NETWORK");
		
		message.setSelf_info(this.node_Info);
		String json=new JSONArray().fromObject(message).toString();
		try {
			this.client=new UDPClient();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.client.sendPacket(Gateway_Port, json);
	}

	@Override
	public void chat(String content, String tag) {
		try {
			this.client=new UDPClient();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ChatItem chatItem=new ChatItem();
		chatItem.setChatContent(content);
		chatItem.setOrigin_tag(this.node_Info.getTag());
		int target_NodeID=hashCode(tag);
		Node_Info routingNode=this.getRoutingNode(this.Routing_Table, target_NodeID);
		PastryMessage message=new PastryMessage();
		message.setTarget_ID(target_NodeID);
		message.setType("CHAT");
		message.setChatItem(chatItem);
		String json=new JSONArray().fromObject(message).toString();
		this.client.sendPacket(routingNode.getPort(), json);
		
		
	}
	private synchronized Node_Info getRoutingNode(Node_Info[] array,int key){
		int i=Integer.MAX_VALUE;
		Node_Info ClosetNode=new Node_Info();
		for(int o=0;o<this.ROUTING_TABLE_SIZE;o++){
			
			if(array[o]!=null){
//				System.out.println(o);
			int temp=Math.abs(key-array[o].getNodeID());
			if(temp<i){
				i=temp;
				ClosetNode=array[o];
				}
			}
		}
		
		return ClosetNode;
	}

	@Override
	public List<ChatItem> getChat(String tag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void leaveNetwork() {
		
		PastryMessage message=new PastryMessage();
		message.setType("LEAVING_NETWORK");
		message.setSelf_info(node_Info);
		message.setRouting_table(Routing_Table);
		String json=new JSONArray().fromObject(message).toString();
		for(int i=0;i<Node.ROUTING_TABLE_SIZE;i++){
			if(Routing_Table[i]!=null){
				try {
					this.client=new UDPClient();
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.client.sendPacket(Routing_Table[i].getPort(), json);
			}
		}
		System.out.println(node_Info.getTag()+" has been left!");
		
	}
	
	public int hashCode(String str) {
		  int hash = 0;
		  for (int i = 0; i < str.length(); i++) {
		    hash = hash * 31 + str.charAt(i);
		  }
		  return Math.abs(hash);
	}
	
	private class UDPServer implements Runnable{
		
		private DatagramSocket HomeSocket;
		private ExecutorService ThreadPool;
		
		public UDPServer(int port) throws SocketException{
			this.HomeSocket=new DatagramSocket(port);
			this.ThreadPool=Executors.newCachedThreadPool();
		}

		@Override
		public void run() {
			
			
			while(true){
				byte[] receiveData = new byte[1024];
				try{
					DatagramPacket receivePacket = new
							DatagramPacket(receiveData, receiveData.length);
					HomeSocket.receive(receivePacket);
					
					this.ThreadPool.execute(new Worker(new String(receivePacket.getData())));
				}catch(IOException e){
					e.printStackTrace();
				}
			}
			
			
		}
		
		
	}
	
	private class Worker implements Runnable{
		
		private String content;
		private PastryMessage message;
		private UDPClient client;
		
		public Worker(String packet_Content) throws SocketException, UnknownHostException{
			this.content=packet_Content;
			
		}

		@Override
		public void run() {
			message=json2java(this.content);
			if(message.getType()!=null){
				switch(message.getType()){
				case "JOINING_NETWORK": join();
										break;
				case "JOINING_NETWORK_RELAY": joinRelay();
												break;
				case "ROUTING_INFO": mergeRoutingTable();
										break;
				case "CHAT": chat();
								break;
				case "LEAVING_NETWORK": leave();
										break;
				default: break; 					
				}
			}
			
			
		}
		private synchronized void leave() {
			
			for(int i=0;i<Node.ROUTING_TABLE_SIZE;i++){
				if(Routing_Table[i]!=null){
					if(Routing_Table[i].getNodeID()==this.message.getSelf_info().getNodeID()){
						Routing_Table[i]=null;
					}
				}
			}
			for(int i=0;i<Node.ROUTING_TABLE_SIZE;i++){
				if (this.message.getRouting_table()[i]!=null){
					this.addNode2RoutingTable(Routing_Table, this.message.getRouting_table()[i]);
					
				}
			}
			
		}

		private synchronized void chat() {
			if(this.message.getTarget_ID()==node_Info.getNodeID()){
				System.out.println("Message arrived Destination!");
				System.out.println(this.message.getChatItem().getOrigin_tag()+" tell "
						+node_Info.getTag()+" : "+this.message.getChatItem().getChatContent());
			}else{
				System.out.println("Massage is passing by "+node_Info.getTag());
				try {
					this.client=new UDPClient();
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String json=new JSONArray().fromObject(this.message).toString();
				Node_Info routing_Node=this.getClosetNode(Routing_Table, this.message.getTarget_ID());
				this.client.sendPacket(routing_Node.getPort(), json);
			}
			
		}

		private synchronized void mergeRoutingTable(){
			Node_Info[] route_Array=this.message.getRouting_table();
			for(int i=0;i<Node.ROUTING_TABLE_SIZE;i++){
				if(route_Array[i]!=null){
					this.addNode2RoutingTable(Routing_Table, route_Array[i]);
				}
			}
			if(this.message.getSelf_info()!=null){
				try {
					this.client=new UDPClient();
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				PastryMessage msg2=new PastryMessage();
				msg2.setRouting_table(Routing_Table);
				msg2.setType("ROUTING_INFO");
				String json=new JSONArray().fromObject(msg2).toString();
				this.client.sendPacket(this.message.getSelf_info().getPort(), json);
			}
			
			
		}
		private synchronized void joinRelay()  {
			Node_Info OriginNode =this.message.getSelf_info();
			PastryMessage msg2=new PastryMessage();
			msg2.setType("ROUTING_INFO");
			msg2.setSelf_info(node_Info);
			msg2.setRouting_table(Routing_Table);
			String json=new JSONArray().fromObject(msg2).toString();
			try {
				this.client=new UDPClient();
			} catch (SocketException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (UnknownHostException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			this.client.sendPacket(OriginNode.getPort(), json);
			
			if(this.message.getTarget_ID()==node_Info.getNodeID()){
				
			}else{
				json=new JSONArray().fromObject(this.message).toString();
				Node_Info LeafNode=getClosetNode(Routing_Table, this.message.getTarget_ID());
				try {
					this.client=new UDPClient();
				} catch (SocketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				this.client.sendPacket(LeafNode.getPort(), json);
			}
			this.addNode2RoutingTable(Routing_Table, this.message.getSelf_info());
			
			
//			for(int i=0;i<4;i++){
//				
//				if(Routing_Table[i]!=null)
//				System.out.println(Routing_Table[i]);
//			}
		}

		private synchronized void join(){
			Node_Info ClosetNode=getClosetNode(allNodes,message.getSelf_info().getNodeID());
			allNodes.add(message.getSelf_info());
			PastryMessage msg2=new PastryMessage();
			msg2.setSelf_info(this.message.getSelf_info());
			msg2.setTarget_ID(ClosetNode.getNodeID());
			msg2.setType("JOINING_NETWORK_RELAY");
			Node_Info leafNode=getClosetNode(Routing_Table,ClosetNode.getNodeID());
			String json=new JSONArray().fromObject(msg2).toString();
			try {
				this.client=new UDPClient();
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(leafNode.getNodeID()==0){
				
			}else{
				this.client.sendPacket(leafNode.getPort(), json);
			}
			this.addNode2RoutingTable(Routing_Table, this.message.getSelf_info());
			
			
			
		}
		
		private synchronized Node_Info getClosetNode(List<Node_Info> list, int key){
			int i=Integer.MAX_VALUE;
			Node_Info CloestNode=new Node_Info();
			for(Node_Info o:list){
				int temp=Math.abs(key-o.getNodeID());
				if(temp<i){
					i=temp;
					CloestNode=o;
				}
			}
			return CloestNode;
		}
		private synchronized Node_Info getClosetNode(Node_Info[] array,int key){
			int i=Integer.MAX_VALUE;
			Node_Info ClosetNode=new Node_Info();
			for(int o=0;o<Node.ROUTING_TABLE_SIZE;o++){
				
				if(array[o]!=null){
//					System.out.println(o);
				int temp=Math.abs(key-array[o].getNodeID());
				if(temp<i){
					i=temp;
					ClosetNode=array[o];
					}
				}
			}
			
			return ClosetNode;
		}
		
		private synchronized void addNode2RoutingTable(Node_Info[] array,Node_Info node){
			if(node.getNodeID()!=node_Info.getNodeID()){
			int i=node.getNodeID()-node_Info.getNodeID();
			if(i<0){
				if(array[1]==null){
					array[1]=node;
				}else{
					if(array[0]==null){
						array[0]=node;
					}else{
						if(node.getNodeID()<array[0].getNodeID()
								||node.getNodeID()==array[0].getNodeID()
								||node.getNodeID()==array[1].getNodeID()){
							
						}else{
							if(node.getNodeID()>array[0].getNodeID()&&node.getNodeID()<
									array[1].getNodeID()){
								array[0]=node;
							}else{
								array[0]=array[1];
								array[1]=node;
							}
						}
					}
				}
			}
			else{
				if(array[2]==null){
					array[2]=node;
				}else{
					if(array[3]==null){
						array[3]=node;
					}else{
						if(node.getNodeID()>array[3].getNodeID()
								||node.getNodeID()==array[2].getNodeID()
								||node.getNodeID()==array[3].getNodeID()){
							
						}else{
							if(node.getNodeID()<array[3].getNodeID()
									&&node.getNodeID()>array[2].getNodeID()){
								array[3]=node;
							}else{
								array[3]=array[2];
								array[2]=node;
							}
						}
					}
				}
			}
			}
		}
		
		
		private PastryMessage json2java(String content){
			List<PastryMessage> list=getJavaCollection(new PastryMessage(),content);
			return list.get(0);
		}
		private <T> List<T> getJavaCollection(T clazz, String jsons) {
	        List<T> objs=null;
	        JSONArray jsonArray=(JSONArray)JSONSerializer.toJSON(jsons);
	        if(jsonArray!=null){
	            objs=new ArrayList<T>();
	            List list=(List)JSONSerializer.toJava(jsonArray);
	            for(Object o:list){
	                JSONObject jsonObject=JSONObject.fromObject(o);
	                T obj=(T)JSONObject.toBean(jsonObject, clazz.getClass());
	                objs.add(obj);
	            }
	        }
	        return objs;
	    }

	}

	
	

}
