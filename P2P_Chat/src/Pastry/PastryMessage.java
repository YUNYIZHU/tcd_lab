package Pastry;

import java.util.List;

public class PastryMessage {
	private String Type;
	private int Target_ID;
	private Node_Info self_info;
	private Node_Info[] Routing_table;
	private ChatItem chatItem;
	
	public ChatItem getChatItem() {
		return chatItem;
	}
	public void setChatItem(ChatItem chatItem) {
		this.chatItem = chatItem;
	}
	public Node_Info[] getRouting_table() {
		return Routing_table;
	}
	public void setRouting_table(Node_Info[] routing_table) {
		Routing_table = routing_table;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	
	public int getTarget_ID() {
		return Target_ID;
	}
	public void setTarget_ID(int target_ID) {
		Target_ID = target_ID;
	}
	public Node_Info getSelf_info() {
		return self_info;
	}
	public void setSelf_info(Node_Info self_info) {
		this.self_info = self_info;
	}
	

}
