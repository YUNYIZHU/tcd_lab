package Pastry;

import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.List;

public interface PeerChat {
	public void init(int Home_Port,String tag)throws SocketException,UnknownHostException;
	public void joinNetwork(int Gateway_Port,String Gateway_tag);
	public void chat(String content, String tag);
	public List<ChatItem> getChat(String tag);
	public void leaveNetwork();
	

}
