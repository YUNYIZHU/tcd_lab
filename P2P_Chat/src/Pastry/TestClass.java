package Pastry;

import java.net.SocketException;
import java.net.UnknownHostException;

import net.sf.json.JSONArray;

public class TestClass {
	public static void main(String[] args) throws InterruptedException{
		Node node1=new Node();
		Node node2=new Node();
		Node node3=new Node();
		Node node4=new Node();
		Node node5=new Node();
		Node node6=new Node();
		Node node7=new Node();
		Node node8=new Node();
		Node node9=new Node();
		try {
			node1.init(9999, "a");
			new Thread().sleep(100);
			node2.init(8998, "zh");
			node2.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node3.init(9998, "b");
			node3.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node4.init(9997, "ch");
			node4.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node5.init(9996, "d");
			node5.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node6.init(9995, "f");
			node6.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node7.init(9994, "z");
			node7.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node8.init(9993, "x");
			node8.joinNetwork(9999,"a");
			new Thread().sleep(100);
			node9.init(9992, "j");
			node9.joinNetwork(9999,"a");
			new Thread().sleep(100);
			
			System.out.println("Network's Initiation finish!");
			System.out.println("==========================");
			
			node6.chat("hello", "ch");
			
			new Thread().sleep(100);
			node9.chat("haha","zh");
			System.out.println("==========================");
			
			new Thread().sleep(100);
			System.out.println("==========================");
			node8.leaveNetwork();
			System.out.println("==========================");
			new Thread().sleep(100);
			node6.chat("hello", "ch");
			
//			
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
