package Pastry;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class UDPClient {
	
	private  InetAddress IPAddress;
	private DatagramSocket ClientSocket;
	private  byte[] sendData;

	
	public UDPClient() throws SocketException, UnknownHostException{
		this.ClientSocket=new DatagramSocket();
		IPAddress=InetAddress.getLocalHost();
		sendData = new byte[1024];
		
	}
	public void sendPacket(int targetPort,String msg){
		this.sendData=msg.getBytes();
		
		DatagramPacket packet=new DatagramPacket(sendData, sendData.length,this.IPAddress
				,targetPort);
		try {
			this.ClientSocket.send(packet);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			this.ClientSocket.close();
		}
	}
}
