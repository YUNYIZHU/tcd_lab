# README #
Fot the lab3, you can start chat server with ServerLauncher.java directly, the port number has been specified to 6789.


P2P_Chat

In this project, I implement Pastry to be the P2P algorithm. In orde to simplify the routing table, the project only achieves leaf set to be
the routing table. Besides this, all Pastry protocols are implemented in the project. 

The test class has been done in the project. I build a network which contains 9 P2P nodes. 
One node would be the bootstrap node. 

When one node wants to chat with another node, the chat message should pass through one path.
all nodes in this path would claim that "message is passing by tag of the node".
Once the message arrives target node, the node will declare that "Message arrived Destination" and "tag of node1 tell tag of node2: xxxxx".

When one node in the path has been left network, it will inform nodes in its routing table, then these nodes will update their routing table. As a consequence, the old path would be discarded. 
